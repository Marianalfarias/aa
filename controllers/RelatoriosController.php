<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;

 
class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }
 

   public function actionRelatorio1()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'select nome from am_cliente order by nome',
            ]
        );
        
        return $this->render('relatorio1', ['resultado' => $consulta]);
   }

   public function actionRelatorio2()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'select datanasc from am_cliente;
        select avg(year(now())-year(datanasc)) from am_cliente',
            ]
        );
        
        return $this->render('relatorio2', ['resultado' => $consulta]);
   }

   public function actionRelatorio3()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'select count(*) especialidade from am_colaborador',
            ]
        );
        
        return $this->render('relatorio3', ['resultado' => $consulta]);
   }

   public function actionRelatorio4()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'select telefone from am_colaborador where especialidade='buffet'',
            ]
        );
        
        return $this->render('relatorio4', ['resultado' => $consulta]);
   }

   public function actionRelatorio5()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'select data from am_agenda where am_cliente_id="1"',
            ]
        );
        
        return $this->render('relatorio5', ['resultado' => $consulta]);
   }

   

}
