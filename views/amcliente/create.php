<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Amcliente */

$this->title = 'Create Amcliente';
$this->params['breadcrumbs'][] = ['label' => 'Amclientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amcliente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
