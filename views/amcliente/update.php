<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Amcliente */

$this->title = 'Update Amcliente: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Amclientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="amcliente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
