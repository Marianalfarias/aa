<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Amcolaborador */

$this->title = 'Create Amcolaborador';
$this->params['breadcrumbs'][] = ['label' => 'Amcolaboradors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amcolaborador-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
