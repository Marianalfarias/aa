<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Amcolaborador */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="amcolaborador-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endereco')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'especialidade')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model,'especialidae')->dropDownList([
    '1'=>'Bolos',
    '2'=>'Doces'
    '3'=>'Buffet'
    '4'=>'Dias ornamentação'
    ],
    ['prompt' => 'Selecione uma especialidade'] );

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
