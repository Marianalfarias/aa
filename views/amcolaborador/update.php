<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Amcolaborador */

$this->title = 'Update Amcolaborador: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Amcolaboradors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="amcolaborador-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
