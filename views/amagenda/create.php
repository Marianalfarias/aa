<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Amagenda */

$this->title = 'Create Amagenda';
$this->params['breadcrumbs'][] = ['label' => 'Amagendas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amagenda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
