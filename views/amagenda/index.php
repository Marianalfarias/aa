<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AmagendaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agenda';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amagenda-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Reservar Data', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'data',
                'filterInputOptions' => [
                    'type'=>'date',
                ],
            ],
            'horario',
            ['attribute'=>'am_cliente.nome','label'=>'Cliente'],
            ['attribute'=>'am_colaborador.nome','label'=>'Colaborador'],
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
