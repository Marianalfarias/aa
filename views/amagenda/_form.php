<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Amagenda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="amagenda-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'data')->textInput() ?>

    <?= $form->field($model, 'horario')->textInput() ?>

    <?= $form->field($model, 'am_cliente_id')->textInput() ?>

    <?= $form->field($model, 'am_colaborador_id')->textInput() ?>
    
    <?= $form->field($model,'opção')->dropDownList([
    '1'=>'Tipo de festa',
    '2'=>'Data'
    '3'=>'Horário'
    '4'=>'Dias disponíveis'
    ],
    ['prompt' => 'Selecione uma opção'] );
 ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
