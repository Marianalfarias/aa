<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "am_cliente".
 *
 * @property int $id
 * @property string $nome
 * @property string $endereco
 * @property string $datanasc
 *
 * @property AmAgenda[] $amAgendas
 */
class Amcliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'am_cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['datanasc'], 'safe'],
            [['nome', 'endereco'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'endereco' => 'Endereco',
            'datanasc' => 'Datanasc',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmAgendas()
    {
        return $this->hasMany(AmAgenda::className(), ['am_cliente_id' => 'id']);
    }
}
