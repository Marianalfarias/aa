<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Amagenda;

/**
 * AmagendaSearch represents the model behind the search form of `app\models\Amagenda`.
 */
class AmagendaSearch extends Amagenda
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'am_cliente_id', 'am_colaborador_id'], 'integer'],
            [['data', 'horario'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Amagenda::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'data' => $this->data,
            'horario' => $this->horario,
            'am_cliente_id' => $this->am_cliente_id,
            'am_colaborador_id' => $this->am_colaborador_id,
        ]);

        return $dataProvider;
    }
}
