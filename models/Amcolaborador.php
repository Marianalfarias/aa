<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "am_colaborador".
 *
 * @property int $id
 * @property string $nome
 * @property string $endereco
 * @property string $telefone
 * @property string $especialidade
 *
 * @property AmAgenda[] $amAgendas
 */
class Amcolaborador extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'am_colaborador';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome', 'telefone'], 'required'],
            [['nome', 'endereco'], 'string', 'max' => 200],
            [['telefone'], 'string', 'max' => 20],
            [['especialidade'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'endereco' => 'Endereco',
            'telefone' => 'Telefone',
            'especialidade' => 'Especialidade',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmAgendas()
    {
        return $this->hasMany(AmAgenda::className(), ['am_colaborador_id' => 'id']);
    }
}
