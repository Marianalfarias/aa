<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "am_agenda".
 *
 * @property int $id
 * @property string $data
 * @property string $horario
 * @property int $am_cliente_id
 * @property int $am_colaborador_id
 *
 * @property AmCliente $amCliente
 * @property AmColaborador $amColaborador
 */
class Amagenda extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'am_agenda';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data', 'horario'], 'safe'],
            [['am_cliente_id', 'am_colaborador_id'], 'integer'],
            [['am_cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmCliente::className(), 'targetAttribute' => ['am_cliente_id' => 'id']],
            [['am_colaborador_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmColaborador::className(), 'targetAttribute' => ['am_colaborador_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'horario' => 'Horario',
            'am_cliente_id' => 'Am Cliente ID',
            'am_colaborador_id' => 'Am Colaborador ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmCliente()
    {
        return $this->hasOne(AmCliente::className(), ['id' => 'am_cliente_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmColaborador()
    {
        return $this->hasOne(AmColaborador::className(), ['id' => 'am_colaborador_id']);
    }
}
